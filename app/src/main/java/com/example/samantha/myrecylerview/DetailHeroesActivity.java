package com.example.samantha.myrecylerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailHeroesActivity extends AppCompatActivity {

    private ImageView imageDetail;
    private TextView sinopsis, tahunDetail;
    private int image;
    private String getSinopsis, getTahun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_heroes);

        //inisialisasi komponen dari layout
        findID();

        //parsing data
        getSinopsis = getIntent().getStringExtra("sinopsis");
        getTahun = getIntent().getStringExtra("tahun");
        image = getIntent().getIntExtra("gambar", 00);

        //mengeset data yang masuk dari parsingan
        imageDetail.setImageResource(image);
        sinopsis.setText(getSinopsis);
        tahunDetail.setText(getTahun);
    }

    private void findID() {
        imageDetail = findViewById(R.id.imageView_detail);
        sinopsis = findViewById(R.id.textView_sinopsis);
        tahunDetail = findViewById(R.id.textView_tahun_detail);
    }
}
