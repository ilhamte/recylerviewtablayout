package com.example.samantha.myrecylerview;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.samantha.myrecylerview.APIHelper.ApiClient;
import com.example.samantha.myrecylerview.APIHelper.ApiInterface;
import com.example.samantha.myrecylerview.APIHelper.CategoriesItems;
import com.example.samantha.myrecylerview.APIHelper.Items;
import com.example.samantha.myrecylerview.adapter.RVFoodsAdapter;
import com.example.samantha.myrecylerview.adapter.SliderAdapter;
import com.example.samantha.myrecylerview.model.CategoriesModel;
import com.example.samantha.myrecylerview.model.FoodsModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class FoodsFragment extends Fragment {

    List<CategoriesModel> categoriesModelList;
    RecyclerView recyclerViewSlider;
    SliderAdapter sliderAdapter;
    private RecyclerView recyclerView;
    private List<FoodsModel> foodsModelList;
    private RVFoodsAdapter rvFoodsAdapter;


    public FoodsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_foods, container, false);


        recyclerView = view.findViewById(R.id.recyclerViewFoods);
        recyclerViewSlider = view.findViewById(R.id.recyclerView_Slider);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        setUpSlider();


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Items> call = apiService.getData();
        call.enqueue(new Callback<Items>() {
            @Override
            public void onResponse(Call<Items> call, Response<Items> response) {

                foodsModelList = response.body().getFoodsModelList();
                rvFoodsAdapter = new RVFoodsAdapter(getActivity(), foodsModelList);
                recyclerView.setAdapter(rvFoodsAdapter);

            }

            @Override
            public void onFailure(Call<Items> call, Throwable t) {
                Log.wtf("hasil", t.getMessage());
            }
        });


        // Inflate the layout for this fragment
        return view;
    }

    private void setUpSlider() {
        //pembuatan objek baru dari class Hero Model
        categoriesModelList = new ArrayList<>();


        //pembuatan objek baru dari class DCAdapter
        sliderAdapter = new SliderAdapter(getActivity(), categoriesModelList);

        //konfigurasi recylerview
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewSlider.setLayoutManager(layoutManager);
        recyclerViewSlider.setHasFixedSize(true);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<CategoriesItems> call = apiService.getCategories();
        call.enqueue(new Callback<CategoriesItems>() {
            @Override
            public void onResponse(Call<CategoriesItems> call, Response<CategoriesItems> response) {

                categoriesModelList = response.body().getCategoriesModelList();
                sliderAdapter = new SliderAdapter(getActivity(), categoriesModelList);
                recyclerViewSlider.setAdapter(sliderAdapter);

            }

            @Override
            public void onFailure(Call<CategoriesItems> call, Throwable t) {
                Log.wtf("hasil", t.getMessage());
            }
        });

    }

}
