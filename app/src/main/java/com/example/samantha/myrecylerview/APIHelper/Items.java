package com.example.samantha.myrecylerview.APIHelper;

import com.example.samantha.myrecylerview.model.FoodsModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Items {

    @SerializedName("meals")
    private List<FoodsModel> foodsModelList;

    public Items(List<FoodsModel> foodsModelList) {
        this.foodsModelList = foodsModelList;
    }

    public List<FoodsModel> getFoodsModelList() {
        return foodsModelList;
    }
}

