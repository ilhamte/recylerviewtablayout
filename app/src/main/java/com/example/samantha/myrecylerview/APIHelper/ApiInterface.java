package com.example.samantha.myrecylerview.APIHelper;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("latest.php")
    Call<Items> getData();

    @GET("categories.php")
    Call<CategoriesItems> getCategories();
}
