package com.example.samantha.myrecylerview.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.samantha.myrecylerview.CategoriesFragment;
import com.example.samantha.myrecylerview.FoodsFragment;

public class TabFoodAdapter extends FragmentPagerAdapter {

    private int numOfTabs;

    public TabFoodAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FoodsFragment();
            case 1:
                return new CategoriesFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}

