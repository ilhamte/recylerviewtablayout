package com.example.samantha.myrecylerview.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.samantha.myrecylerview.DetailHeroesActivity;
import com.example.samantha.myrecylerview.R;
import com.example.samantha.myrecylerview.model.HeroModel;

import java.util.List;

public class DCAdapter extends RecyclerView.Adapter<DCAdapter.ViewHolder> {
    private Context mContext;
    private List<HeroModel> heroModelList;

    public DCAdapter(Context mContext, List<HeroModel> heroModelList) {
        this.mContext = mContext;
        this.heroModelList = heroModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.layout_dc_heroes, viewGroup, false);
        DCAdapter.ViewHolder vHolder = new DCAdapter.ViewHolder(v);

        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        viewHolder.judul.setText(heroModelList.get(position).getJudul_film());
        viewHolder.tahun.setText(heroModelList.get(position).getTahun());
        viewHolder.gambar.setImageResource(heroModelList.get(position).getGambar_film());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailHeroesActivity.class);
                intent.putExtra("sinopsis", heroModelList.get(position).getSinopsis());
                intent.putExtra("tahun", heroModelList.get(position).getTahun());
                intent.putExtra("gambar", heroModelList.get(position).getGambar_film());
                v.getContext().startActivity(intent);

                Toast.makeText(mContext, "" + heroModelList.get(position).getJudul_film(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return heroModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView judul;
        private TextView tahun;
        private ImageView gambar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            judul = itemView.findViewById(R.id.textView_judul);
            tahun = itemView.findViewById(R.id.textView_tahun);
            gambar = itemView.findViewById(R.id.imageView);
        }
    }
}
